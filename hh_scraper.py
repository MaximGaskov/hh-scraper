from bs4 import BeautifulSoup
import requests
import sys
import argparse
import json
import pandas as pd

URL = 'https://spb.hh.ru/search/vacancy'
HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'}
AREA = 2 #Saint-Petersburg
ITEMS_ON_PAGE = 100
ORDER = 'relevance'
EXCEL_FILE = 'search_results.xlsx'
CSV_FILE = 'search_results.csv'

def string_is_int(string):
    try:
        int(string)
        return True
    except:
        return False

def parse_cmd_args():
    '''returns dictionary of command line arguments'''
    query_help_info = "a query string, if Q contains more then one word " \
                  "then it must be in quotes: '<query_string>'"
    exp_help_info = "a job experience string from the list below:\n" \
                    " * no experience: 'noExperience'\n" \
                    " * 1-3 years: 'between1And3'\n" \
                    " * 3-6 years: 'between3And6'\n" \
                    " * > 6 years: 'moreThan6'\n" \
                    " * experience doesn't matter: 'doesNotMatter'\n" 
    choices_list = ['noExperience', 'between1And3', 'between3And6',
                    'moreThan6', 'doesNotMatter']

    args_parser = argparse.ArgumentParser(
                            description='Fetch jobs information from HeadHunter.com',
                            formatter_class=argparse.RawTextHelpFormatter)
    args_parser.add_argument('query', metavar='Q', type=str, 
                             help=query_help_info) 
    args_parser.add_argument('-e', type=str, default='doesNotMatter',
                             choices=choices_list, help=exp_help_info)

    return vars(args_parser.parse_args())


def count_pages(query_params):
    '''scraps page quantity from first html page returned'''
    params = {
        'area': AREA, 
        'text': query_params['query'],
        'experience': query_params['e'],
        'items_on_page': ITEMS_ON_PAGE,
        'order_by': ORDER,
        'search_field': 'name',
        'page': 0
    }
    
    response = requests.get(URL, headers=HEADERS, params=params)
    if response.status_code != 200:
        return 0

    soup = BeautifulSoup(response.text, 'lxml')
    
    # check if there are page buttons on the page
    if soup.find('a', class_='HH-Pager-Control'):
        button_page_numbers = []
        for bpn in soup.findAll('a', class_='HH-Pager-Control'):
            if string_is_int(bpn.text):
                button_page_numbers.append(int(bpn.text))
        return max(button_page_numbers)
    # if no page buttons then only one page
    else:
        return 1


def fetch_html_pages(query_params, pages_num):
    '''returns list of html pages recieved from hh server'''
    params = {
        'area': AREA,
        'text': query_params['query'],
        'experience': query_params['e'],
        'items_on_page': ITEMS_ON_PAGE,
        'order_by': ORDER,
        'search_field': 'name',
        'page': 0
    }
    pages = []
   
    for p in range(pages_num):
          params['page'] = p
          response = requests.get(URL, headers=HEADERS, params=params) 
          pages.append(response.text)
    
    return pages

def scrape_vacancies(query_params, pages):
    params = {
        'area': AREA,
        'text': query_params['query'],
        'experience': query_params['e'],
        'items_on_page': ITEMS_ON_PAGE,
        'page': 0
    }
    
    soup = BeautifulSoup(pages[0], 'lxml')
    if not soup.find('div', class_='vacancy-serp-item'):
        print('no vacancies found')
        exit(2)

    data = []

    for page in pages:
        soup = BeautifulSoup(page, 'lxml')
        for vacancy in soup.findAll('div', class_='vacancy-serp-item'):
            name_a = vacancy.find('span', class_='g-user-content').a
            salary_div = vacancy.find('div',
                                    class_='vacancy-serp-item__compensation')
            
            link = name_a['href']
            name = name_a.text
            company = vacancy.find('a', {'data-qa':
                                   'vacancy-serp__vacancy-employer'}).text.strip()
            salary = None
            if salary_div:
                salary = salary_div.text
           
            data.append((company, name, salary, link))

    data_frame = pd.DataFrame(data=data)
    data_frame.columns = ['Company', 'Vacancy', 'Salary', 'Link']
    data_frame.to_csv(CSV_FILE, index=False)
    data_frame.to_excel(EXCEL_FILE)
    print(f'{len(data_frame.index)} vacancies found')

cmd_args = parse_cmd_args()
pages = fetch_html_pages(cmd_args, count_pages(cmd_args))
scrape_vacancies(cmd_args, pages)
